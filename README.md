# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This is repository contains a trial plugin for WPMU
* Version
1.0.0

### How do I get set up? ###

* Summary of installation
Upload plugin's zip from Dashboard > Plugins > Add New.
Select the plugin's zip and click "Upload"

* Configuration/Settings
From Dashboard > Settings > Options for inactive sites
Set the number of days that a subsite should not be upldated in order to be considered inactive

Then the plugin offers the user with an interface to create a series of actions to be taken.

Three types of actions are included
1. 'Message' : Sends a message to the subsite owner to inform about the situation and perhaps about the actions the super admin is about to make

2. 'Wait' : Adds a waiting period (in days) where nothing happens to the subsite.

3. 'Change Status' : Changes the status of the sub site to 'Unpublished' or 'Archived' or 'Deleted'

These three types can be combined in any way. Here is a possible scenario:

1. After 15 days a site is considered 'inactive'
2. At the 15th day there is message send which informs about the situation.
3. Then there is a waiting period for another 5 days.
4. If still inactive we send another message warning that his site will become 'Unpublished'.
5. Then there is another waiting period for another 5 days.
6. After these 5 days if still inactive, subsite status will become unpublished and a message will be sent saying that in another 3 days it will be deleted.
etc...



* Dependencies
Needs WordPress Multi site


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact