<?php
/**
*
* Settings available only to network admin
* 
* @since 1.0.0
*
*/

class WPMU_Trial_Settings{


	public function __construct() {

        add_action('network_admin_menu', array( 'WPMU_Trial_Settings', 'admin_settings_menu' ) );

        add_action('admin_post_update_inactive_sites_options',  array( 'WPMU_Trial_Settings', 'update_options' ) );
 
	}


	public static function admin_settings_menu(){

		add_submenu_page(
	       'settings.php',
	       __( 'Options for inactive sites', 'wpmu-trial' ),
	       __( 'Options for inactive sites', 'wpmu-trial' ),
	       'manage_network_options',
	       'wpmu-inactivity-options',
	       array( 'WPMU_Trial_Settings', 'admin_settings_page' )
  		);   
	}


	public static function admin_settings_page(){

		global $wpmu_trial;

		$wpmu_trial->check_settings_notice_msg_visibility();

		$options = get_site_option('wpmu_trial_inactive_sites');
		
		$actionset = isset( $options['actions'] ) ? $options['actions'] : array();

		$period_opts = array();

        for( $p = 5; $p <= 60; $p+=5 ){
        	$period_opts[$p] = $p;
        }

		?>
		<div class="wrap">
		<h1><?php esc_html_e( 'Options for inactive sites', 'wpmu-trial' ); ?></h1>

		<form action="<?php echo admin_url('admin-post.php?action=update_inactive_sites_options'); ?>" method="post">

			<?php wp_nonce_field('wpmu_inactive_sites_nonce'); ?>

			<table id="menu" class="form-table">

				<tr valign="top">

					<th scope="row">
						<span data-hint="<?php _e( 'How many days should a site remain inactive before taking action?', 'wpmu-trial' ); ?>" class="hint hint--right hint--rounded">
							<?php _e( 'Waiting Period', 'wpmu-trial' ); ?> <i class="dashicons dashicons-editor-help"></i>
						</span>
					</th>

	                <td>

		                <select name="wpmu_trial_inactive_sites[wpmu_trial_waiting_period]" id="wpmu_trial_waiting_period">

		                <?php foreach ( $period_opts as $period ) { ?>
		                	<option value="<?php echo $period; ?>" <?php echo $period == $options['wpmu_trial_waiting_period'] ? 'selected' : ''; ?> ><?php echo $period; ?></option>
		                <?php } ?>
		                
		                </select>
		                <?php _e( ' days', 'wpmu-trial' ); ?>
		                

	                </td>

				</tr>

				<tr valign="top">

					<th scope="row" class="wpmumis_actions_list_title_wrap">						
						<span data-hint="<?php _e( 'Here you can set a series of actions to take place when a subsite is considered to be inactive. 
						Just click on the &quot;Add new action&quot; button and select one of the available actions.', 'wpmu-trial' ); ?>" class="hint hint--right hint--rounded">
							<?php _e( 'Set the order of actions', 'wpmu-trial' ); ?> <i class="dashicons dashicons-editor-help"></i>
						</span>
					</th>

					<td id="wpmumis_actionslist_col">

						<h4 class="wpmumis_section_title"><?php _e( 'List of actions', 'wpmu-trial' ); ?></h4>

						<div id="wpmumis_actionslist_wrap">
							<div id="wpmumis_actionslist">

								<?php $empty_actions_list_class = 'show'; ?>

								<?php if( is_array( $actionset ) && !empty( $actionset ) ): ?>

									<?php $empty_actions_list_class = 'hidden'; ?>

									<?php foreach( $actionset as $counter => $action ): ?>

										<?php //print_r( $action ); ?>
										<?php echo $wpmu_trial->load_action( $action['type'], $counter, $action['value'] ); ?>

									<?php endforeach; ?>

								<?php endif;?>

								<div class="empty_actionsset_message <?php echo $empty_actions_list_class; ?>">
									<h3 class="title"><?php _e( 'There are no options set for inactive sites', 'wpmu-trial' ); ?></h3>
									<p>
										<?php _e( 'Please click the "Add new action" button and select the actions you want to take for inactive subsites', 'wpmu-trial' ); ?>
									</p>
								</div>

							</div>

							<div id="wpmumis_add_new_action_wrap">
								
								<a id="wpmumis_add_new_action_btn">
									<i class="dashicons dashicons-plus"></i>            
									<span><?php _e( 'Add new action', 'wpmu-trial' ); ?></span>
								</a>

							</div>

						</div>

					</td>

				</tr>



	        </table> 

	        <input type="submit" value="<?php _e( 'Save', 'wpmu-trial' ); ?>" class="button button-primary" />
		</form>
		</div>


		<div id="wpmumis_actions_selector_popup" class="wpmumis_popup">

			<div class="wpmumis_popup_head">
				
				<h3 class="title"> <?php _e( 'Choose next action', 'wpmu-trial' ); ?> </h3>

			</div>

			<div class="popup_content">
				<p>
					<?php _e( 'Choose an action from the dropdown: ', 'wpmu-trial' ); ?>
					<select id="wpmumis_action_types_list">
						<option value="status_change"><?php _e( 'Change status', 'wpmu-trial' ); ?></option>
						<option value="message"><?php _e( 'Send message', 'wpmu-trial' ); ?></option>
						<option value="wait"><?php _e( 'Wait (Give the user some time)', 'wpmu-trial' ); ?></option>
					</select>

					<br />
					<?php _e( 'Then click the "OK" button to add it to your list.', 'wpmu-trial' ); ?>

					<br />
					<?php _e( 'Once you have added an action to your list, you can set additional options.', 'wpmu-trial' ); ?>

				</p>

			</div>

			<div class="wpmumis_popup_button_wrap">
				<p class="wpmumis_popup_button_wrap">
					<a class="wpmumis_ok_btn" title="<?php _e( 'OK', 'wpmu-trial' ); ?>">
						<i class="dashicons dashicons-yes"></i> 
						<?php _e( 'OK', 'wpmu-trial' ); ?>
					</a>
					<a class="wpmumis_cancel_btn" title="<?php _e( 'Cancel', 'wpmu-trial' ); ?>">
						<i class="dashicons dashicons-no-alt"></i>
						<?php _e( 'Cancel', 'wpmu-trial' ); ?>
					</a>
				</p>
			</div>

		</div>
		<?php

	}



    /**
    * Update Settings
    */

    public static function update_options(){

    	check_admin_referer('wpmu_inactive_sites_nonce');

    	if(!current_user_can('manage_network_options')) wp_die('No sufficient permissions');
		
		$inactive_opts_vals = $_POST['wpmu_trial_inactive_sites'];

		update_site_option( 'wpmu_trial_inactive_sites', $inactive_opts_vals );

		wp_redirect(admin_url('network/settings.php?page=wpmu-inactivity-options&updated=true'));


    }


}

new WPMU_Trial_Settings;
?>