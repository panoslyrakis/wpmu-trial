<div class="action_type_item wait_action_type" data-action-type="wait">

	<table>
		<tr>
			<td class="counter_wrap">
				<span class="counter_num"><?php echo $action_counter; ?></span>
			</td>
			<th class="action_type_col">
				<?php _e( 'Set how many days to wait until your next action', 'wpmu-trial' ); ?>
			</th>
			<td class="action_val_col">
				<input type="hidden" class="action_val_type" name="wpmu_trial_inactive_sites[actions][<?php echo $action_counter; ?>][type]" value="wait">

				<input type="number" class="action_val" step="1" name="wpmu_trial_inactive_sites[actions][<?php echo $action_counter; ?>][value]" value="<?php echo $value; ?>">
				
			</td>
			<td class="action_rm_col">
				<a class="remove_action_type" data-action-counter="<?php echo $action_counter; ?>">
					<i class="dashicons dashicons-dismiss"></i>
				</a>
			</td>
		</tr>
	</table>

</div>