<?php
 $plain_text = __('Hi there [user_full_name],&#013;&#010;&#013;&#010;It seems like your site ([subsite_name]) is not that active since it has been several days we have last seen you.&#013;&#010;&#013;&#010;We would like to inform you that your site will be Unpublished/Archived/Deleted in some days from today.&#013;&#010;&#013;&#010;So please visit your site and keep it alive!&#013;&#010;&#013;&#010;Kind regards,&#013;&#010;[website_name] Team', 'wpmu-trial' );

 $txt = $value != '' ? stripcslashes( $value ) : $plain_text;
?>
<div class="action_type_item message_action_type" data-action-type="message">

	<table>
		<tr>
			<td class="counter_wrap">
				<span class="counter_num"><?php echo $action_counter; ?></span>
			</td>
			<th class="action_type_col">
				<?php _e( 'Send the following message:', 'wpmu-trial' ); ?>
			</th>
			<td class="action_val_col">
				<input type="hidden" class="action_val_type" name="wpmu_trial_inactive_sites[actions][<?php echo $action_counter; ?>][type]" value="message">

				<textarea class="action_val" name="wpmu_trial_inactive_sites[actions][<?php echo $action_counter; ?>][value]"><?php esc_html_e( $txt ); ?></textarea>
				
			</td>
			<td class="action_rm_col">
				<a class="remove_action_type" data-action-counter="<?php echo $action_counter; ?>">
					<i class="dashicons dashicons-dismiss"></i>
				</a>
			</td>
		</tr>
	</table>

</div>