<div class="action_type_item status_change_action_type" data-action-type="status_change">

	<table>
		<tr>
			<td class="counter_wrap">
				<span class="counter_num"><?php echo $action_counter; ?></span>
			</td>
			<th class="action_type_col">
				<?php _e( 'Set a new status', 'wpmu-trial' ); ?>
			</th>
			<td class="action_val_col">
				<input type="hidden" class="action_val_type" name="wpmu_trial_inactive_sites[actions][<?php echo $action_counter; ?>][type]" value="status_change">
				<select class="action_val" name="wpmu_trial_inactive_sites[actions][<?php echo $action_counter; ?>][value]">
					<option value="archived" <?php echo $value=='archived' ? 'selected' : ''; ?> ><?php _e( 'Archived', 'wpmu-trial' ); ?></option>
					<option value="unpublished" <?php echo $value=='unpublished' ? 'selected' : ''; ?> ><?php _e( 'Unpublished', 'wpmu-trial' ); ?></option>
					<option value="deleted" <?php echo $value=='deleted' ? 'selected' : ''; ?> ><?php _e( 'Mark Deleted', 'wpmu-trial' ); ?></option>
					<option value="hard_delete" <?php echo $value=='hard_delete' ? 'selected' : ''; ?> ><?php _e( 'Delete completely', 'wpmu-trial' ); ?></option>
				</select>
			</td>
			<td class="action_rm_col">
				<a class="remove_action_type" data-action-counter="<?php echo $action_counter; ?>">
					<i class="dashicons dashicons-dismiss"></i>
				</a>
			</td>
		</tr>
	</table>

</div>