var $ = jQuery.noConflict();

$( document ).ready(function(){

	$( '#wpmumis_actions_selector_popup' ).popup({
        transition: 'all 0.3s'
    });


    $( '#wpmumis_add_new_action_btn' ).on( 'click', function(){

    	wpmumis_set_available_actions();

    	$( '#wpmumis_actions_selector_popup' ).popup('show');    	
    	
    });



    $( '.wpmumis_ok_btn' ).on( 'click', function(){
    	
    	var selected_action = $( '#wpmumis_action_types_list' ).val()
    	
    	wpmumis_load_new_action( selected_action );

    	$( '#wpmumis_actions_selector_popup' ).popup('hide');
    	$('.empty_actionsset_message').fadeOut( 300 );

    });


    $( '.wpmumis_cancel_btn' ).on( 'click', function(){
    	$( '#wpmumis_actions_selector_popup' ).popup('hide');
    });

    $( '#wpmumis_actionslist' ).delegate( '.remove_action_type', 'click', function(){

    	var actions_counter = $( '.action_type_item' ).length + 1;
    	var action_number = $( this ).data('action-counter') - 1;

    	$( '.action_type_item:eq('+action_number+')' ).fadeOut( 300, function(){ $( this ).remove(); wpmumis_reset_items_numbering(); });

    });

});


function wpmumis_load_new_action( selected_action ){

	var actions_counter = $( '.action_type_item' ).length + 1;

	var data = {
        action: 'wpmumis_load_action',
        security: WPMUMIS_GLOBAL.nonce,
        selected_action: selected_action,
        action_counter: actions_counter
    };

	$.ajax({
		url:ajaxurl,
		type:"POST",
		data: data,
		success:function(response){

			$( '#wpmumis_actionslist' ).append( response );

		},
		error: function( xhr, status, error ){
		    console.log(xhr.responseText);
		}
	});

}


function wpmumis_reset_items_numbering(){

	var actions_counter = $( '.action_type_item' ).length;

	if( actions_counter > 0 ){

		$('.empty_actionsset_message').fadeOut( 300 );

		var counter = 1;
		$( '.action_type_item' ).each( function(){
			$( this ).find( '.counter_wrap .counter_num' ).html( counter );
			$( this ).find( '.remove_action_type' ).attr( 'data-action-counter', counter );			
			$( this ).find( '.action_val_type' ).attr( 'name', 'wpmu_trial_inactive_sites[actions]['+counter+'][type]' );
			$( this ).find( '.action_val' ).attr( 'name', 'wpmu_trial_inactive_sites[actions]['+counter+'][value]' );
			counter++;
		});

	}
	else{
		$('.empty_actionsset_message').fadeIn( 300 );
	}
	
}


function wpmumis_set_available_actions(){

	var prev_action = $( '.action_type_item' ).last().data( 'action-type' );

	$('#wpmumis_action_types_list').children().attr('disabled', false);

	if( typeof prev_action == 'undefined' || prev_action == '' || prev_action == 'wait' ) $('#wpmumis_action_types_list').children('option[value="wait"]').attr('disabled', true);

}