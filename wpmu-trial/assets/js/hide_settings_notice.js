var $ = jQuery.noConflict();

$( document ).ready(function(){

		
	$( '#wpmumsi_visit_settings_notice' ).delegate( '.notice-dismiss', 'click', function(e){
		
		e.preventDefault();


		var data = {
	        action: 'wpmumis_remove_settings_notice',
	        security: WPMUMIS_GLOBAL.nonce
	    };

		$.ajax({
			url:ajaxurl,
			type:"POST",
			data: data,
			success:function(response){

				if( response == '_OK_' )
					$( '#wpmumsi_visit_settings_notice' ).fadeOut( 300, function(){ $( this ).remove() });

			},
			error: function( xhr, status, error ){
			    console.log(xhr.responseText);
			}
		});

	});

});