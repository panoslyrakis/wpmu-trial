<?php
/*
Plugin Name: Manage Inactive Subsites
Plugin URI: http://github.org
Description: This is a quick and simple plugin for WordPress multisite that will allow a super admin to automate what happens to inactive subsites on a public-signup multisite network.
Author: Panos Lyrakis
Version: 1.0.0
Author URI: http://no-site.org/
Text Domain: wpmu-trial
Domain Path: /languages/
Network: true 
*/


if ( !defined( 'ABSPATH' ) ) exit();


define( 'WPMUTRIAL_DIR_PATH', plugin_dir_path( __FILE__ ) );
define( 'WPMUTRIAL_DIR_URI',  plugin_dir_url( __FILE__ ) );



/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
*/

add_action( 'plugins_loaded', 'wpmu_trial_load_textdomain' );

function wpmu_trial_load_textdomain() {
    load_plugin_textdomain( 'wpmu-trial', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}




/* When plugin is activated */
register_activation_hook( __FILE__, 'wpmu_trial_activation' );

function wpmu_trial_activation( $network_wide ){
	 
	global $wpdb, $current_site;

	if ( ! is_multisite() || !$network_wide ) {
		
		$e_plugin_title = '<strong>'. __( 'Manage Inactive Subsites', 'wpmu-trial' ) .'</strong>';
		$e_msg = sprintf( __( 'The "%s" plugin requires a WordPress Multisite configuration in order to function', 'wpmu-trial' ), $e_plugin_title );
		$e_msg .= '<p>'. sprintf( __( 'Consider visting <a href="%s" target="_blank">this page<a> to learn how to make your WordPress installation Multisite.' ), 'http://codex.wordpress.org/Create_A_Network' ) .'</p>';


		if ( isset( $_GET['action'] ) && $_GET['action'] == 'error_scrape' ) {

			echo $e_msg;

			exit;

		} else {

			trigger_error( $e_msg, E_USER_ERROR );

		}
	
	}

	// Just to make sure we create a cron job for the main blog (although activation can happen only for main blog...)
	if ( !is_multisite() || !is_main_site( $current_site->blog_id ) ) return;

	wp_schedule_event( time(), 'daily', 'wpmu_trial_inactive_sites_cron' );

	if( !get_site_option( 'wpmu_trial_inactive_sites' ) ){

		$default_opts = array(
			'wpmu_trial_waiting_period' => 5
		);

		add_site_option( 'wpmu_trial_inactive_sites', $default_opts );
	}


	require_once ABSPATH . 'wp-admin/includes/upgrade.php';

	$charset_collate = '';
	if (!empty($wpdb -> charset)) $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
	if (!empty($wpdb -> collate)) $charset_collate .= " COLLATE $wpdb->collate";

	/**
	* ===== Create the instructions table =====
	* Description
	* 	This table will hold the information about the actions to be made on inactive blogs
	*
	* -------------------------------------------------------------------
	*
	* Columns :
	*   0. Record ID
	* 	1. Site ID
	* 	2. ins_date holds the date_time the action took place
	* 	3. step holds a number indicating which part/step of the whole actions list (set at the settings page) we are. This way we can figure out which is the next action
	* 	4. action_type will hold the type of the action that happened (Wait, Message or Status change). For use in the admin dashboard list.
	*	5. action_value similarly as 4. only that it contains the value
	*	6. action_status signifies if the action is Pending (for the Wait action) or Completed. Perhaps we do not need this...
	*/

	$query = "CREATE TABLE %s (
		id bigint(20) unsigned NOT NULL auto_increment,
		site_id int(11) NOT NULL,
		ins_date DATETIME NOT NULL default '0000-00-00 00:00:00',
		step int(3) NOT NULL,
		action_type varchar(13) NOT NULL,
		action_value text NOT NULL,
		action_status varchar(9) NOT NULL,
		PRIMARY KEY  (id),
		KEY site_id (site_id),
		KEY action_status (action_status)
    ) $charset_collate;";

	
	dbDelta(sprintf($query, $wpdb->base_prefix.'wpmu_mis_instructions'));

}



//When plugin is deactivated remove chron job and reset the message for visiing settings
register_deactivation_hook( __FILE__, 'wpmu_trial_deactivation' );

function wpmu_trial_deactivation(){

	//Reshow the "Visit Settings" nag on ReActivation
	global $current_user;

	$user_id = $current_user->ID;

	delete_user_meta($user_id, 'wpmumis_ignore_notice');

	//Remove the cron
	wp_clear_scheduled_hook( 'wpmu_trial_inactive_sites_cron' );

	//delete_option( 'wpmu_trial_shown_message' );

}


/* When plugin is uninstalled */
register_uninstall_hook( __FILE__, 'wpmu_trial_uninstall' );

function wpmu_trial_uninstall(){

	global $wpdb;

	//Remove the plugin's option
	delete_option( 'wpmu_trial_inactive_sites' );

	//Drop plugin's table
	$instructions_table = $wpdb->base_prefix.'wpmu_mis_instructions';
	$wpdb->query( "DROP TABLE IF EXISTS {$instructions_table}" );

	//Don't forget to remove the Cron AGAIN...

}





class WPMU_Trial{

	public function __construct(){

		add_action( 'plugins_loaded', array( $this, 'load_files' ), 30 );

		//Run the cron
		add_action( 'wpmu_trial_inactive_sites_cron', array( 'WPMU_Trial', 'check_inactive_sites' ) );

		//Styles & Scripts
		add_action( 'admin_print_styles', array( 'WPMU_Trial', 'admin_styles' ) );
		add_action( 'admin_print_scripts', array( 'WPMU_Trial', 'admin_scripts' ) );

		//Add a dashboard widget
		add_action( 'wp_network_dashboard_setup', array( 'WPMU_Trial', 'dashboard_widgets' ) );

		//Ajax calls
		//Load actions ins settings
		add_action('wp_ajax_wpmumis_load_action', array( 'WPMU_Trial', 'ajax_load_action' ) );
		//Remove notice
		add_action('wp_ajax_wpmumis_remove_settings_notice', array( 'WPMU_Trial', 'ajax_rm_notice' ) );


		//Info box
		add_action('network_admin_notices', array( 'WPMU_Trial', 'settings_notice_msg' ) );

		//Hide info box when visiting the settings page
		add_action('current_screen', array( 'WPMU_Trial', 'check_settings_notice_msg_visibility' ) );

		//Add settings link
		add_filter( 'network_admin_plugin_action_links_' . plugin_basename(__FILE__), array( 'WPMU_Trial', 'add_action_links' ) );

	}


	public static function add_action_links( $links ){

		$settings_link = array(
		 '<a href="' . network_admin_url( 'settings.php?page=wpmu-inactivity-options' ) . '">'. __( 'Settings', 'wpmu-trial' ) .'</a>',
		 );
		return array_merge( $links, $settings_link );

	}



	public static function load_files(){

		require_once WPMUTRIAL_DIR_PATH . 'inc/settings.php';

	}



	public static function dashboard_widgets(){

		wp_add_dashboard_widget(
                 'wpmumsi_dashboard_widget',
                 __( 'Inactive Sites History', 'wpmu-trial' ),
                 array( 'WPMU_Trial', 'inactive_sites_history' )
        );

	}



	public static function inactive_sites_history() {

		global $wpdb;

		$instructions_table = $wpdb->base_prefix.'wpmu_mis_instructions';

		$actions_hist_q = "SELECT * FROM {$instructions_table} order by id desc limit 10";
        
        $actions_hist = $wpdb->get_results($actions_hist_q);

        if( !empty( $actions_hist ) ){

        	?>
        	<table class="history-table">
        		<thead>
		        	<tr>
		        		<th>

		        		</th>

		        		<th>
		        			<?php _e( 'Site', 'wpmu-trial' ); ?>
		        		</th>

		        		<th>
		        			<?php _e( 'Action', 'wpmu-trial' ); ?>
		        		</th>

		        		<th>
		        			<?php _e( 'Action Status', 'wpmu-trial' ); ?>
		        		</th>

		        		<th>
		        			<?php _e( 'Date', 'wpmu-trial' ); ?>
		        		</th>

		        	</tr>
	        	</thead>

	        	<?php $counter = 1; ?>

	        	<?php foreach( $actions_hist as $action_hist ): ?>

	        	<?php $blog_info = get_blog_details( $action_hist->site_id ); ?>

	        	<tbody>
		        	<tr>

		        		<td>
		        			<?php echo $counter; ?>
		        		</td>

		        		<td>
		        			<?php _e( $blog_info->blogname ); ?>
		        		</td>

		        		<td>
		        			<?php _e( WPMU_Trial::display_action_title( $action_hist->action_type ) ); ?>
		        		</td>

		        		<td>
		        			<?php echo $action_hist->action_status; ?>
		        		</td>

		        		<td>
		        			<?php echo $action_hist->ins_date; ?>
		        		</td>

		        	</tr>
	        	<tbody>

	        	<?php $counter++; ?>

	        	<?php endforeach; ?>
        	</table>
        	<?php

        }
        else{

        	?>
        	<div>
        		<?php _e( 'There have been no actions for inactive sites yet', 'wpmu-trial' ); ?>
        	</div>
        	<?php

        }

        
	}



	public static function display_action_title( $action_code ){

		$action_title = '';

		switch( $action_code ){

			case 'wait' : $action_title = __( 'AWAITING', 'wpmu-trial' ); break;

			case 'status_change' : $action_title = __( 'STATUS CHANGE', 'wpmu-trial' ); break;

			case 'message' : $action_title = __( 'SEND MESSAGE', 'wpmu-trial' ); break;

		}

		return $action_title;

	}



	public static function admin_styles(){


		$screen = get_current_screen();

		//Settings style
		if( $screen->id == 'settings_page_wpmu-inactivity-options-network' ){

			wp_register_style( 'wpmumis_settings_style', WPMUTRIAL_DIR_URI.'assets/css/settings.css' );
	        wp_enqueue_style( 'wpmumis_settings_style' );

	        wp_register_style( 'wpmumis_popup_style', WPMUTRIAL_DIR_URI.'assets/popup/jquery.popupoverlay.css' );
	        wp_enqueue_style( 'wpmumis_popup_style' );

	        wp_register_style( 'wpmumis_hint_style', WPMUTRIAL_DIR_URI.'assets/css/hint.css' );
	        wp_enqueue_style( 'wpmumis_hint_style' );

	    }

	    //Dashborad Style
	    if( $screen->id == 'dashboard-network' ){

	    	wp_register_style( 'wpmumis_dashboard_style', WPMUTRIAL_DIR_URI.'assets/css/dashboard.css' );
	        wp_enqueue_style( 'wpmumis_dashboard_style' );

	    }

	}


	public static function admin_scripts(){

		global $current_user ;

        

		$screen = get_current_screen();

		$localizedparams = array(
			'nonce'		=> wp_create_nonce('wpmumis_ajax-nonce')
		);

		//Settings scripts
		if( $screen->id == 'settings_page_wpmu-inactivity-options-network' ){

			wp_enqueue_script( 'wpmumis_globalz_js', WPMUTRIAL_DIR_URI.'assets/js/globalz.js', array(), '1.0.0', false );
      		wp_localize_script( 'wpmumis_globalz_js', 'WPMUMIS_GLOBAL', $localizedparams );

			wp_register_script( 'wpmumis_popup_js', WPMUTRIAL_DIR_URI.'assets/popup/jquery.popupoverlay.js' );
	        wp_enqueue_script( 'wpmumis_popup_js' );
	
	        wp_register_script( 'wpmumis_settings_js', WPMUTRIAL_DIR_URI.'assets/js/settings.js' );
	        wp_enqueue_script( 'wpmumis_settings_js' );
	    }


	    $user_id = $current_user->ID;

        if ( ! get_user_meta($user_id, 'wpmumis_ignore_notice') ) {

        	wp_enqueue_script( 'wpmumis_globalz_js', WPMUTRIAL_DIR_URI.'assets/js/globalz.js', array(), '1.0.0', false );
      		wp_localize_script( 'wpmumis_globalz_js', 'WPMUMIS_GLOBAL', $localizedparams );

        	wp_register_script( 'wpmumis_hide_settings_notice_js', WPMUTRIAL_DIR_URI.'assets/js/hide_settings_notice.js' );
	        wp_enqueue_script( 'wpmumis_hide_settings_notice_js' );

        }

	}


	public static function settings_notice_msg(){

		global $current_user ;

        $user_id = $current_user->ID;

        if ( ! get_user_meta($user_id, 'wpmumis_ignore_notice') ) {
		
			echo '<div class="updated notice is-dismissible" id="wpmumsi_visit_settings_notice" style="border-left-color: #FFBA00;">';//00A0D2

				echo '<p>';
		        	printf(__('Visit the <a href="%1$s">settings</a> page to manage the actions to take for inactive sub sites.', 'wpmu-trial' ), network_admin_url( 'settings.php?page=wpmu-inactivity-options' ) );
		        echo "</p>";

	        echo "</div>";

	    }


	    if( !is_multisite() ){

	    	echo '<div class="error notice" id="wpmumsi_not_mu_notice">';

				echo '<p>';
		        	printf(__('The "Manage Inactive Sites" plugin can only be used for Multisite WordPress installations. Currently it can not be used by this site.', 'wpmu-trial' ), network_admin_url( 'settings.php?page=wpmu-inactivity-options' ) );
		        echo "</p>";

	        echo "</div>";

	    }

	}


	public static function check_settings_notice_msg_visibility(){

        $screen = get_current_screen();

		//Settings style
		if( $screen->id == 'settings_page_wpmu-inactivity-options-network' ){

             //add_user_meta($user_id, 'wpmumis_ignore_notice', 'true', true);
			WPMU_Trial::ignore_settings_notice();

    	}

	}



	public static function ignore_settings_notice(){

		global $current_user;

		$user_id = $current_user->ID;

		add_user_meta($user_id, 'wpmumis_ignore_notice', 'true', true);

		return true;

	}




	public static function check_inactive_sites(){

		global $wpdb;

		if( !is_multisite() ) return false;

		//Some of our site's info that we will need
		$main_site = array(
			'name' => get_site_option( 'site_name' ),
			'email' => get_site_option( 'admin_email' )
		);

		//We have to check the 'last_update' date of all blogs
		$blogs = wp_get_sites();

		$options = get_site_option('wpmu_trial_inactive_sites');

		$actions_set = isset( $options['actions'] ) ? $options['actions'] : array();

		if( !is_array( $actions_set ) || empty( $actions_set ) ) return;

		//This is our custom table.
		$instructions_table = $wpdb->base_prefix.'wpmu_mis_instructions';

		//How many days to consider a blog inactive
		$inactive_days_limit = (int)$options['wpmu_trial_waiting_period'];

		$today_dt = new DateTime( date('Y-m-d H:i:s') );

		//Array to store blog_ids to be removed from the "Black list"
		$remove_blogs_from_inactivity = array();

		foreach( $blogs as $blog ){

			$blog_id = $blog['blog_id'];

			$last_update = $blog['last_updated'];

			$last_update_dt = new DateTime($last_update);

			//$date_diff = $today_dt->diff($last_update_dt);

			$date_diff = date_diff( $today_dt, $last_update_dt, true );
		    $date_diff = (int)$date_diff->format('%a');
			
			//We NEVER consider our main site as inactive
			if( is_main_site( $blog_id ) ) continue;
			
			//Check if the limit is reached so the site is considered inactive
			if( $date_diff >= $inactive_days_limit ){

				//Check if the blog exists in instructions table
				// 		if it does, 
				//			a. execute instruction
				//			b. mark executed instruction as 'completed' action
				//			c. store next 'wait' instruction if exists as 'pending'
				//		if it doesn't,
				//			a. execute current instruction(s) (from the $actions_set array) & keep executing until we find a 'wait'
				//			b. store next 'wait' instruction if exists in the $actions_set


				$exists_sql = "SELECT * FROM {$instructions_table} WHERE site_id=%d order by id desc";
		        $exists_sql = $wpdb->prepare(
		            $exists_sql,
		            $blog_id
		        );

		        $instruction = $wpdb->get_row($exists_sql);

		        // Check if exists in instructions table	
		        if( is_object( $instruction ) && !empty( $instruction ) ){
		        	
		        	//The date of the previous 'wait'
		        	$last_ins = new DateTime($instruction->ins_date);

		        	//Current date
		        	$now = new DateTime( current_time('mysql', true) );

		        	//The step of the previous 'wait' (in the $actions_set)
		        	//we need this so we can find out which is the next action to take place according to the $actions_set
		        	$prev_step = (int)$instruction->step;

		        	//The number of the next action in the $actions_set
		        	$next_step = $prev_step + 1;

		        	//SO, if there is a 'wait' instruction, we need to check if it is time to execute next action
		        	//else we will wait... and do nothing
		        	if( $instruction->action_type == 'wait' ){

		        		$D_diff = date_diff( $now, $last_ins, true );
		        		$last_date_diff = (int)$D_diff->format('%a');


		        		if( isset( $actions_set[$next_step] ) && $action = $actions_set[$next_step] ){

		        			$prev_waiting_days = $instruction->action_value;

		        			$all_actions_count = count($actions_set);
		        			
		        			if( $prev_waiting_days <= $last_date_diff ){

								for( $st = $next_step; $st <= $all_actions_count; $st++ ){

									$next_instruction = $actions_set[$st];
	
				        			$action = array(
						        		'type' => $next_instruction['type'],
						        		'value' => $next_instruction['value'],
						        		'step' => $st
						        	);
						        	
						        	$wpdb->update( $instructions_table, array( 'action_status' => 'completed' ), array( 'id' => $instruction->id ), array( '%s' ), array( '%d' ) );

						        	$execution_resp = WPMU_Trial::execute_action( $action, $blog, $main_site );

		        					if( $execution_resp == '_exec_wait_' ) break;

								}
			        			

		        			}
		        		}
		        	}

		        	
		        }
		        else{

		        	//Start executing first actions from $actions_set
		        	foreach( $actions_set as $action_num => $action ){

		        		$action['step'] = $action_num;

		        		$execution_resp = WPMU_Trial::execute_action( $action, $blog, $main_site );

		        		// If there is a wait, then we should proceed to the next subsite
		        		if( $execution_resp == '_exec_wait_' ) break;

		        	}


		        }

			}
			else{
				//Here is what happens if the blog is not concidered inactive
				$remove_blogs_from_inactivity[] = $blog_id;
			}



		}//End of foreach looping blogs

		if( !empty( $remove_blogs_from_inactivity ) ){

			foreach( $remove_blogs_from_inactivity as $rm_blog_id ){

				$wpdb->delete( $instructions_table, array( 'site_id' => $rm_blog_id ), array( '%d' ) );

			}

		}


	}



	public static function execute_action( $action, $blog, $main_site ){

		if( !is_array( $action ) || empty( $action ) || !is_array( $blog ) || empty( $blog ) ) return 'exec_next';

		global $wpdb;
		$instructions_table = $wpdb->base_prefix.'wpmu_mis_instructions';

		$blog_id = $blog['blog_id'];
		$blog_name = get_blog_option( $blog_id, 'blogname' );
			
		$users = get_users( 'blog_id='.$blog_id.'&role=administrator' );

		$type = $action['type'];
		$value = $action['value'];

		$return_msg = 'exec_next';

		switch( $type ){
			case 'message':
				$msg = str_replace( '[subsite_name]', $blog_name, $value );
				$msg = str_replace( '[website_name]', $main_site['name'], $msg );
				$headers = 'From: '. $main_site['name'] . ' <' . $main_site['email'] . '>' . "\r\n";
				
				foreach( $users as $user ){
					$msg = str_replace( '[user_full_name]',$user->display_name, $msg );
					mail( $user->user_email, __( 'Notice of Site Inactivity', 'wpmu-trial' ), $msg, $headers );

				}

				$wpdb->insert( 
		        		$instructions_table, 
		        		array(
		        			'site_id' => $blog_id,
		        			'action_type' => 'message',
		        			'action_value' => $msg,
		        			'action_status' => 'completed',
		        			'ins_date' => current_time('mysql', true),
		        			'step' => $action['step']
		        		),
		        		array( '%d', '%s', '%s', '%s', '%s', '%d' )
		        	);
				
				break;

			case 'status_change':

				//wpmu_delete_blog located in ms.php and has not yet loaded so we need to include it
				require_once ABSPATH . 'wp-includes/pluggable.php';
				require_once ABSPATH . 'wp-admin/includes/ms.php';

				if( $value == 'archived' )
					$wpdb->update( $wpdb->blogs, array( 'archived' => 1 ), array('blog_id' => $blog_id) );

				if( $value == 'unpublished' )
					$wpdb->update( $wpdb->blogs, array( 'public' => 0 ), array('blog_id' => $blog_id) );

				if( $value == 'deleted' ) 
					$wpdb->update( $wpdb->blogs, array( 'deleted' => 1 ), array('blog_id' => $blog_id) );
					//wpmu_delete_blog( $blog_id ); // This also changes the last_update date... so we do it custom as the rest

				if( $value == 'hard_delete' ) {
					wpmu_delete_blog( $blog_id, true );
					$wpdb->delete( $instructions_table, array( 'site_id' => $blog_id ), array( '%d' ) );
					$return_msg = '_exec_wait_';
				}

				
				//Save action in the instructions table
				if( $value != 'hard_delete' ){
					//On hard_delete there is no point in saving it in the instructions table

					$wpdb->insert( 
			        		$instructions_table, 
			        		array(
			        			'site_id' => $blog_id,
			        			'action_type' => 'status_change',
			        			'action_value' => $value,
			        			'action_status' => 'completed',
			        			'ins_date' => current_time('mysql', true),
			        			'step' => $action['step']
			        		),
			        		array( '%d', '%s', '%s', '%s', '%s', '%d' )
			        	);

				}

				break;

			case 'wait':

				$wpdb->insert( 
		        		$instructions_table, 
		        		array(
		        			'site_id' => $blog_id,
		        			'action_type' => 'wait',
		        			'action_value' => $value,
		        			'action_status' => 'pending',
		        			'ins_date' => current_time('mysql', true),
		        			'step' => $action['step']
		        		),
		        		array( '%d', '%s', '%s', '%s', '%s', '%d' )
		        	);

				$return_msg = '_exec_wait_';

				break;
		}


		return $return_msg;

	}



	public static function ajax_load_action(){

		check_ajax_referer( 'wpmumis_ajax-nonce', 'security' );

		$selected_action = $_POST['selected_action'];

		$action_counter = (int)$_POST['action_counter'];

		echo WPMU_Trial::load_action( $selected_action, $action_counter );

		exit;

	}


	public static function ajax_rm_notice(){

		check_ajax_referer( 'wpmumis_ajax-nonce', 'security' );

		if( WPMU_Trial::ignore_settings_notice() ) echo '_OK_';
		else echo '_ERROR_';
		
		exit;
	}



	public static function load_action( $action_type, $action_counter, $value = '' ){

		if( $action_type == '' ) return;

		$template = '';

		switch( $action_type ){

			case 'status_change' : $template = 'status_change.tpl.php'; break;
			case 'message' : $template = 'message.tpl.php'; break;
			case 'wait' : $template = 'wait.tpl.php'; break;

		}


		ob_start();
		include WPMUTRIAL_DIR_PATH . 'inc/templates/' . $template;
		$html = ob_get_clean();

		return $html;

	}

}


$wpmu_trial = new WPMU_Trial;
//$wpmu_trial->check_inactive_sites();
?>